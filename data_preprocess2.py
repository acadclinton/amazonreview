## -*- coding: utf-8 -*-
import time
import pandas as pd
import re
import numpy as np
from nltk import word_tokenize, FreqDist

print("load raw data")
data = pd.read_csv('review_table.csv',sep='\tSEP\t', engine='python')
# ratings = np.array(data['rating'])
reviews = data['reviewText']
data = None

print("filter reviews with null values")
validIndex = pd.notnull(reviews)
# ratings = [r for i,r in enumerate(ratings) if validIndex[i]]
reviews = [rev for i,rev in enumerate(reviews) if validIndex[i]]

print "#reviews: ", len(reviews)


for i in range(0,10):
    print reviews[i]



print("preprocess text")
reviews_temp = reviews

regex = re.compile(r"\&\#[0-9]+\;")

for i, rev in enumerate(reviews_temp):
    try:
        # text parsing
        rev = rev.decode("utf-8")
        rev = regex.sub(' ', rev)
        reviews_temp[i] = word_tokenize(rev.lower().replace('.', '. '))
        # collect all tokens to count
    except Exception as inst:
        print type(inst)
        print inst.args
        print(i, reviews[i])

reviews = reviews_temp

for i in range(0,10):
    print reviews[i]

print("Term frequency")
tokens_all = [item for sublist in reviews for item in sublist]
fdist = FreqDist(tokens_all)
words = fdist.keys()
count = fdist.values()
max_features = len(words)
print("max features: ", max_features)

print "#words: ", len(words)



print("transform words into numbers")
nWords = 50000

#dic = fdist.most_common(nWords)
# reviews = "\n".join(" "+" ".join(r)+" " for r in reviews)
# for i, w in enumerate(words):
#     reviews = reviews.replace(" "+str(w)+" "," "+str(i)+" ")
# reviews = [r.strip().split(" ") for r in reviews.split("\n")]


#print fdist

dic_aux = fdist.most_common(nWords)

dic = [word for word,count in dic_aux]
#for i in range(0, len(dic_aux)):
#    dic[dic_aux[i][0]] = i+1
dic_set = set(dic)
#dic = dict((w,i) for i,w in enumerate(words,1))
#dic = dict((w,i) for i,w in enumerate(words[:nWords],1))

#print dic


# reviews = [[dic[t] if t in dic.keys() else 0 for t in rev] for rev in reviews]
t = time.time()
print t

reviews_tmp = []

print 'top 10 dic'
for i in xrange(0,10):
    print dic[i]

enu = enumerate(dic)

for rev in reviews:
    count = count+ 1

    if count % 1000 == 0:
    	print count
    rev_temp = []
#    to_check = []
#    to_check2 = []
    for t in rev:
#        to_check = to_check + [t in dic_set]
        # to_check2 = to_check2 + [t in dic]
        if t in dic_set:
        #    to_check2 = to_check + [dic.index(t)+1]
            rev_temp = rev_temp + [dic.index(t)+1]
        #    for i,x in enu:
	#        c = False
        #        if x == t:
        #            c = True
        #            rev_temp = rev_temp + [i+1]
        #    to_check2 = to_check2 + [c]
        else:
            rev_temp = rev_temp + [0]
    reviews_tmp = reviews_tmp + [rev_temp]
    if count <= 10:
#        print 'line %d' % count
#        print rev
#        print to_check
#	print to_check2
        print rev_temp
#    elif count > 1000:
#        break

reviews = reviews_tmp

end = time.time()

print 'time elapsed'
print t
print end

for i in range(0,10):
    print reviews[i]
# for i, w in enumerate(words[:nWords],1):
    # reviews = [[t if t!=w else i for t in rev] for rev in reviews]

print("save as csv")
import csv

with open("reviews3.csv", "wb") as f:
    writer = csv.writer(f, lineterminator='\n')
    for i,r in enumerate(reviews):
    	writer.writerow(r)
        if i>100000:break

# #Assuming res is a list of lists
# with open("reviews.csv", "w") as output:
#     writer = csv.writer(output, lineterminator='\n')
#     writer.writerows(reviews)


# with open("ratings.csv", "wb") as f:
#     writer = csv.writer(f)
#     writer.writerows(ratings)

# with open("myfile.csv", "w") as output:
#     writer = csv.writer(output, lineterminator='\n')
#     for val in ratings:
#         writer.writerow([val])
