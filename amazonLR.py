# coding: utf-8


from __future__ import absolute_import
import numpy as np
import pandas as pd
np.random.seed(1337) # for reproducibility
from keras.preprocessing import sequence
from keras.optimizers import SGD, RMSprop, Adagrad,Adadelta
from keras.utils import np_utils
from keras.models import Sequential
from keras.layers.core import Dense, Dropout, Activation,Flatten
from keras.layers.embeddings import Embedding
from keras.layers.recurrent import LSTM, GRU



data=pd.read_csv('review_table.csv',sep='\tSEP\t')
rating=np.array(data['rating'])
data=None


# In[3]:
#print np.bincount(rating)

max_features = 137620
maxlen = 200 # cut texts after this number of words (among top max_features most common words)
batch_size = 512
nb_classes=5
w=[]
with open('weight.txt','r') as r:
    for line in r:
        t=line.split()
	w.append([float(i) for i in t])
W=np.array(w)
w=None
print W.shape


# In[ ]:

print("Loading data...")
#(X_train, y_train), (X_test, y_test) = imdb.load_data(nb_words=max_features, test_split=0.2)
X_train=[]
X_test=[]
y_train=[]
y_test=[]
X_pool=[]
y_pool=[]
with open('review_table_convert.csv','r') as r:
    cnt=0
    for j,line in enumerate(r):
        t=line.split()
        tt=[int(i) for i in t]
        y=rating[j]
	if len(tt)==0:
            #print(tt)
            #print(cnt)
            continue
	X_pool.append(tt)
	y_pool.append(y-1)
        #print '\r{0}/1000000 ( {1}% ) completed'.format(j, float(j)/10000),
    #print
X_pool=X_pool[:600000]
y_pool=y_pool[:600000]


def BalancedSample(X_pool,y_pool,n_class,n_train,n_valid,n_test):
    X_v=X_pool[:n_valid]
    y_v=y_pool[:n_valid]
    X_te=X_pool[n_valid:n_valid+n_test]
    y_te=y_pool[n_valid:n_valid+n_test]
    X_tr=X_pool[n_valid+n_test:n_valid+n_test+n_train]
    y_tr=y_pool[n_valid+n_test:n_valid+n_test+n_train]
    

    """X_sample=[]
    Y_sample=[]
    X_pool2=X_pool[n_valid+n_test:n_valid+n_test+n_train]

    y_pool2=y_pool[n_valid+n_test:n_valid+n_test+n_train]

    for cl in range(0,n_class):
        mask=np.in1d(y_pool2,[cl])
        X_sample.append(X_pool2[mask])
        Y_sample.append(y_pool2[mask])
        print cl,':',len(X_sample[cl])
    n_train_per_cl=(int)(n_train/n_class)
    X_tr=[]
    y_tr=[]
    for cl in range(0,n_class):
        sample_per_cl=X_sample[cl].shape[0]
        if sample_per_cl>= n_train_per_cl :
            X_tr=X_tr+list(X_sample[cl])[:n_train_per_cl]
            y_tr=y_tr+list(Y_sample[cl])[:n_train_per_cl]

        else:
            X_tr=X_tr+list(X_sample[cl])[:]
            y_tr=y_tr+list(Y_sample[cl])[:]
            sample_index=np.random.choice(np.array(range(0,sample_per_cl)),n_train_per_cl-sample_per_cl,replace=True)
            X_tr=X_tr+list(X_sample[cl][sample_index])
            y_tr=y_tr+list(Y_sample[cl][ sample_index ])"""
            

    return np.array(X_tr),np.array(y_tr),np.array(X_v),np.array(y_v),np.array(X_te),np.array(y_te)    

X_pool=np.array(X_pool)
y_pool=np.array(y_pool)
X_train,y_train,X_valid,y_valid,X_test,y_test=BalancedSample(X_pool,y_pool,5,40000,10000,10000)


for cl in range(0,5):
    mask=np.in1d(y_train,[cl])
    print cl,':',len(X_train[mask]),len(y_train[mask])
    mask=np.in1d(y_valid,[cl])
    print cl,':',len(X_valid[mask]),len(y_valid[mask])

    mask=np.in1d(y_test,[cl])
    print cl,':',len(X_test[mask]),len(y_test[mask])






print("Pad sequences (samples x time)")
X_train = sequence.pad_sequences(X_train, maxlen=maxlen)
X_valid = sequence.pad_sequences(X_valid, maxlen=maxlen)
X_test = sequence.pad_sequences(X_test, maxlen=maxlen)
    

Y_train = np_utils.to_categorical(y_train, nb_classes)
Y_valid = np_utils.to_categorical(y_valid, nb_classes)
Y_test = np_utils.to_categorical(y_test, nb_classes)

print('X_train shape:', X_train.shape)
print('X_test shape:', X_test.shape)
print('y_train shape:', Y_train.shape)
print('y_test shape:', Y_test.shape)

# In[4]:
print('Build model...')

model = Sequential()
#model.add(Embedding(max_features, 200, input_length=maxlen,weights=[W]))
model.add(Embedding(max_features, 200, input_length=maxlen))
model.add(Flatten())
model.add(Dense(nb_classes,W_regularizer='l2'))
model.add(Activation('softmax'))
adadelta=Adadelta(lr=1.0, rho=0.95, epsilon=1e-6)
adagrad=Adagrad(lr=0.01, epsilon=1e-6)
sgd=SGD(lr=0.1, momentum=1e-6, decay=0.9, nesterov=True)
model.compile(loss='categorical_crossentropy', optimizer=adadelta)
#model.load_weights('best.hdf5')




# In[5]:
print("Train...")
max_epoch=1000
i,prev_score=0,100
run_epoch=1
while i<max_epoch:
    i=i+run_epoch
    print i,'/',max_epoch
    model.fit(X_train, Y_train, batch_size=batch_size, nb_epoch=run_epoch, show_accuracy=True)
    score, acc = model.evaluate(X_valid, Y_valid, batch_size=batch_size, show_accuracy=True)
    print 'Score:',score,' Acc:',acc,
    if(score<prev_score):
        print 'Best'
        prev_score=score;
        model.save_weights('best.hdf5', overwrite=True)  
    else:
        print
model.load_weights('best.hdf5')
score, acc = model.evaluate(X_test, Y_test, batch_size=batch_size, show_accuracy=True)
print score,acc



# In[ ]:

