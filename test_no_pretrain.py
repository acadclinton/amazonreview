from __future__ import absolute_import
from __future__ import print_function
import numpy as np
np.random.seed(1337)  # for reproducibility

from keras.preprocessing import sequence
from keras.optimizers import SGD, RMSprop, Adagrad, Adadelta
from keras.utils import np_utils
from keras.models import Sequential
from keras.layers.core import Dense, Dropout, Activation, Flatten, Lambda, TimeDistributedDense
from keras.layers.embeddings import Embedding
from keras.layers.convolutional import Convolution1D
from keras.layers.recurrent import LSTM, GRU
from keras.datasets import imdb
import sys
import pandas as pd


import csv

'''
    Train a LSTM on the IMDB sentiment classification task.
    The dataset is actually too small for LSTM to be of any advantage
    compared to simpler, much faster methods such as TF-IDF+LogReg.
    Notes:
    - RNNs are tricky. Choice of batch size is important,
    choice of loss and optimizer is critical, etc.
    Some configurations won't converge.
    - LSTM loss decrease patterns during training can be quite different
    from what you see with CNNs/MLPs/etc.
    GPU command:
        THEANO_FLAGS=mode=FAST_RUN,device=gpu,floatX=float32 python imdb_lstm.py
'''

max_features = 10001
max_len = 100  # cut texts after this number of words (among top max_features most common words)
min_len = 10

batch_size =512 
valid_split=0.2
test_split=0.2
hidden=32
context=9
if len(sys.argv)>1:
    hidden=int(sys.argv[1])
#with open('word2vec_10000_%i_%i.csv'%(hidden,context),'rb') as f:
#    reader = csv.reader(f)
#    vec = list(reader)
#vec = [np.array([float(item) for item in row]) for row in vec]



print('Build model...')

def one_hot(X):
    return np_utils.to_categorical(X, 50001)

def output_shape():
    return (1,50001)


model = Sequential()
#model.add(TimeDistributedDense(1,input_length=max_len))
#model.add(Lambda(one_hot,output_shape))
#model.add(TimeDistributedDense(256,input_dim=50001,input_length=max_len))
model.add(Embedding(10001, hidden, input_length=max_len))
#model.add(Embedding(50001, 256, input_length=max_len,weights=[vec]))
model.add(LSTM(256,return_sequences=False))  # try using a GRU instead, for fun
model.add(Dropout(0.5))
model.add(Dense(1))
model.add(Activation('sigmoid'))

# try using different optimizers and different optimizer configs
model.compile(loss='binary_crossentropy', optimizer='adam', class_mode="binary")
#model.compile(loss='categorical_crossentropy', optimizer='adam')

#model.compile(loss='categorical_crossentropy', optimizer='adagrad',class_mode="categorical")


## for my computer, loading all data takes about 2min
print("Loading data...")

with open('reviews10000.csv', 'rb') as f:
    reader = csv.reader(f)
    all_X = list(reader)
all_X = [[int(item) for item in row] for row in all_X]

with open('ratings.csv', 'rb') as f:
    reader = csv.reader(f)
    all_y = list(reader)

################################################################
## y modifying part! change here to make it multimodal
all_y = [[int(int(item)<=3) for item in row] for row in all_y]
#all_y = [[int(item) for item in row] for row in all_y]
#all_y = [[int(item)-1 for item in row] for row in all_y]
################################################################


print ('sentence max length: ',max_len)
print ('sentence min length: ',min_len)

### cut off rows that length is longer than max_len and shorter than min_len
proper_ind_list = [i for i in xrange(0,len(all_X)) if (len(all_X[i]) >= min_len and len(all_X[i]) <= max_len)]

all_X = [all_X[i] for i in proper_ind_list]
all_y = [all_y[i] for i in proper_ind_list]

### train set / text set split
valid_index=int(len(all_X)*valid_split)

test_index=int(len(all_X)*test_split)

train_index =len(all_X)-valid_index-test_index 
X_train = all_X[:train_index]
X_valid=all_X[train_index:train_index+valid_index]
X_test = all_X[train_index+valid_index:]
y_train = all_y[:train_index]
y_valid = all_y[train_index:train_index+valid_index]
y_test = all_y[train_index+valid_index:]

print(len(X_train), 'train sequences')
print(len(X_valid), 'valid sequences')
print(len(X_test), 'test sequences')

print("Pad sequences (samples x time)")
X_train = sequence.pad_sequences(X_train, maxlen=max_len)
X_valid = sequence.pad_sequences(X_valid, maxlen=max_len)
X_test = sequence.pad_sequences(X_test, maxlen=max_len)
for cl in range(0,5):
    mask=np.in1d(y_train,[cl])
    print (cl,':',len(X_train[mask]))
    mask=np.in1d(y_valid,[cl])
    print (cl,':',len(X_valid[mask]))
    mask=np.in1d(y_test,[cl])
    print (cl,':',len(X_test[mask]))



Y_train=np_utils.to_categorical(y_train,5)
Y_valid=np_utils.to_categorical(y_valid,5)
Y_test=np_utils.to_categorical(y_test,5)
print('X_train shape:', X_train.shape)
print('X_valid shape:', X_valid.shape)
print('X_test shape:', X_test.shape)
print('Y_train shape:',Y_train.shape)

print("Train...")
min_score=100.0
min_score_filename=''
for i in range(0,10):
    model.fit(X_train, y_train, batch_size=batch_size, nb_epoch=1, show_accuracy=True)
    model.save_weights('%i_%i_%i_no_pretrain_%i.hdf5'%(max_features,hidden,context,i), overwrite=True)  
    score, acc = model.evaluate(X_valid, y_valid, batch_size=batch_size, show_accuracy=True)
    if min_score>score:
        min_score=score
        min_score_filename='%i_%i_no_pretrain_%i.hdf5'%(max_features,hidden,i)
    print('Test score:', score)
    print('Test accuracy:', acc)
print(min_score_filename)
model.load_weights(min_score_filename)
score, acc = model.evaluate(X_test, y_test, batch_size=batch_size, show_accuracy=True)
y_predict=model.predict_classes(X_test)
y_true=[i[0] for i in y_test]
y_pred=[i[0] for i in y_predict]
from sklearn.metrics import precision_score,recall_score,f1_score
print ('Score:',score)
print ('Acc:',acc)
pre=precision_score(y_true, y_pred,average=None)
rec=precision_score(y_true, y_pred,average=None)
f1=f1_score(y_true, y_pred,average=None)
print ('Precision:',pre) 
print ('Recall:',rec) 
print ('F1:',f1)

with open('%i_no_pretrain.result'%(hidden),'w') as w:
    w.write('Score:\t'+str(score)+'\n')
    w.write('Acc:\t'+str(acc)+'\n')
    w.write('Precision:\t'+str(pre)+'\n')
    w.write('Recall:\t'+str(rec)+'\n')
    w.write('F1:\t'+str(f1)+'\n')

