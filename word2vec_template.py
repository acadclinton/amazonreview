# -*- coding: utf-8 -*-
import numpy as np
# import matplotlib.pyplot as plt
import pandas as pd
from pandas import DataFrame, Series
import re
# import nltk
import gensim
import csv

#data 불러오기(sentence 단위)
def read_data(filename):
    with open(filename, 'r') as f:
        reader = csv.reader(f)
        data = list(reader)
    return data


#각 sentence 를 token 단위로 list 만들기
def tokens(sentences):
    token = []
    for i in range(len(sentences)):
        for word in sentences[i]:
            token.append(word)
    return token


import time

print('read start')
total = read_data('reviews2.csv')

for window in [5,7,9]:
    for hidden_size in [512]:
        min_count = 1
        # hidden_size = 256
        workers = 8
        # window= 10
        epoch = 5

        # print('read start')
        # total = read_data('reviews2.csv')
        print('model start: window, hidden -', window, hidden_size)
        start = time.time()
        model = gensim.models.Word2Vec(total, window = window, min_count=min_count, sample=1e-5, size = hidden_size, workers = workers, iter = epoch)
        model.save('model_total')
        end = time.time()
        print( end-start)

        print ('write start')
        with open('word2vec'+'_'+str(hidden_size)+'_'+str(window)+'.csv','wb') as f:
            writer = csv.writer(f)
            for i in xrange(0,50001):
                writer.writerow(model[str(i)])
        


